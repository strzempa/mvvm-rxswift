//
//  NavigationControllerStyleProtocol.swift
//  MVVM+RXSwift
//
//  Created by Patryk Strzemiecki on 20.09.2017.
//  Copyright © 2017 Patryk Strzemiecki. All rights reserved.
//

import UIKit

protocol NavigationControllerStyleProtocol{
    func setClearNavigationControllerStyle()
    func addNavigationControllerReloadButton()
}

extension NavigationControllerStyleProtocol where Self: UIViewController{
    func setClearNavigationControllerStyle(){
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func addNavigationControllerReloadButton(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(FeedViewController.reloadFeedDataSource))
    }
}
