//
//  Item.swift
//  MVVM+RXSwift
//
//  Created by Patryk Strzemiecki on 08.09.2017.
//  Copyright © 2017 Patryk Strzemiecki. All rights reserved.
//

enum ItemType:String{
    case post = "post"
}

import ObjectMapper

struct Item: Mappable{
    var id: String!
    var createdAt: String?
    var updatedAt: String?
    var type: ItemType?
    var payload: Payload?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        createdAt <- map["createdAt"]
        updatedAt <- map["updatedAt"]
        type <- (map["type"],EnumTransform<ItemType>())
        payload <- map["payload"]
    }
}
