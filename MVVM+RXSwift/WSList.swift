//
//  WSList.swift
//  MVVM+RXSwift
//
//  Created by Patryk Strzemiecki on 08.09.2017.
//  Copyright © 2017 Patryk Strzemiecki. All rights reserved.
//

import Alamofire

typealias ListCompletion = (_ success: Bool, _ list: List?, _ error: Error?) -> ()

final class WSList {
    
    private let url = "https://staging.morgenstern.presspadapp.com/v1/app/193c45f5-a509-48ea-affd-1d2678d4605c/articles/list?offset=0&expanded=1"
    
    static let staticInstance = WSList()
    
    func get(_ completion: ListCompletion?){
        
        Alamofire.request(url).responseJSON(completionHandler: { response in
        
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                
                let result = List(JSONString: utf8Text)
                
                completion!(true,result,nil)
            }
            
        })
        
    }
    
}
