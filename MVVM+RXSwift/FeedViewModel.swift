//
//  FeedViewModel.swift
//  MVVM+RXSwift
//
//  Created by Patryk Strzemiecki on 08.09.2017.
//  Copyright © 2017 Patryk Strzemiecki. All rights reserved.
//

import RxSwift


protocol FeedViewModelType {
    func fetchList()
    var list: Variable<[List]> { get }
    var bag: DisposeBag { get }
}

class FeedViewModel: FeedViewModelType{
    var bag: DisposeBag
    var list = Variable<[List]>([])
    
    init() {
        bag = DisposeBag()
        fetchList()
    }
    
    func fetchList(){
        WSList.staticInstance.get({ [weak self] success, list, error in
            if let list = list{
                self?.list.value.append(list)
            }
        })
    }
    
}
