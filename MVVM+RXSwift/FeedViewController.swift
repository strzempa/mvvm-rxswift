//
//  FeedViewController.swift
//  MVVM+RXSwift
//
//  Created by Patryk Strzemiecki on 08.09.2017.
//  Copyright © 2017 Patryk Strzemiecki. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class FeedViewController: UIViewController, NavigationControllerStyleProtocol{
    
    @IBOutlet weak var tableView: UITableView!
    fileprivate var viewModel: FeedViewModelType  = FeedViewModel() as FeedViewModelType
    
    override func viewDidLoad() {
        setClearNavigationControllerStyle()
        addNavigationControllerReloadButton()
        tableView.registerFeedViewCells()
        
        viewModel.list.asObservable().subscribe(onNext: { [weak self] list in
            if list.count > 0{
                self?.tableView.reloadData()
                print("reloadData with: \(list)")
            }
        }).addDisposableTo(viewModel.bag)
    }
    
    func reloadFeedDataSource(){
        viewModel.fetchList()
    }
}

extension FeedViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CustomCell.self), for: indexPath)
        if let cell = cell as? CustomCell{
            guard let item = viewModel.list.value.last?.items?[indexPath.row] else { return UITableViewCell() }
            cell.set(with: item)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let items = viewModel.list.value.last?.items else { title = "Loading..."; return 0 }
        title = "Feed [\(items.count)]"
        return items.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CustomCell.self), for: indexPath)
        if let cell = cell as? CustomCell{
            guard let contents = cell.get(for: viewModel.list.value.last?.items?[indexPath.row]) else { return }
            dump(contents)
        }
    }
}

extension FeedViewController: UITableViewDelegate{


}
