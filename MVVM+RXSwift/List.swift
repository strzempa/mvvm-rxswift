//
//  List.swift
//  MVVM+RXSwift
//
//  Created by Patryk Strzemiecki on 08.09.2017.
//  Copyright © 2017 Patryk Strzemiecki. All rights reserved.
//

import ObjectMapper

struct List: Mappable{
    var offset: Int?
    var limit: Int?
    var total: Int?
    var items: [Item]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        offset <- map["offset"]
        limit <- map["limit"]
        total <- map["total"]
        items <- map["items"]
    }
}
