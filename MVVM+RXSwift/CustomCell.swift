//
//  CustomCell.swift
//  MVVM+RXSwift
//
//  Created by Patryk Strzemiecki on 08.09.2017.
//  Copyright © 2017 Patryk Strzemiecki. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell{
    
    @IBOutlet weak var titleLabel: UILabel!
    
    private var item: Item?
    
    override func awakeFromNib() {
//        titleLabel.transform = CGAffineTransform(scaleX: 0, y: 0) //bounce animation
        titleLabel.transform = CGAffineTransform(translationX: UIScreen.main.bounds.width, y: 0) //fly in animation
    }
    
    func set(with item: Item?){
        guard let item = item else { return }
        self.item = item
        load()
    }
    
    func get(for item: Item?) -> Item?{
        guard let item = item else { return nil }
        return item
    }
    
    func load(){
        titleLabel.text = item?.payload?.title ?? ""
        animate()
    }
    
    func animate(){
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.1, options: [.curveEaseInOut,.allowUserInteraction], animations: { [weak self] in
            self?.titleLabel.transform = .identity
            }, completion: nil)
    }
}
