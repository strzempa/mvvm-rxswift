//
//  TableViewExtension.swift
//  MVVM+RXSwift
//
//  Created by Patryk Strzemiecki on 20.09.2017.
//  Copyright © 2017 Patryk Strzemiecki. All rights reserved.
//

import UIKit

extension UITableView{
    func registerFeedViewCells(){
        self.register(CustomCell.self, forCellReuseIdentifier: String(describing: CustomCell.self))
        self.register(UINib(nibName: String(describing: CustomCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CustomCell.self))
    }
}
